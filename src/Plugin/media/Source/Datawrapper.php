<?php

namespace Drupal\media_entity_datawrapper\Plugin\media\Source;

use Drupal\media\Plugin\media\Source\OEmbed;

/**
 * Datawrapper media entity source definition.
 *
 * @MediaSource(
 *   id = "datawrapper",
 *   label = @Translation("Datawrapper"),
 *   description = @Translation("Embed chart visualizations from Datawrapper."),
 *   providers = {"Datawrapper"},
 *   allowed_field_types = {"string"},
 * )
 */
class Datawrapper extends OEmbed {

}
